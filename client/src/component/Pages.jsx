import React, { Component } from 'react'
import ReactTable from "react-table-v6";
import '../../src/sass/Table.css'

export default class pages extends Component {
    constructor(props) {
		super(props);
		this.state = {
            data : []
        };
    }
    fetchItems() {
        // Where we're fetching data from
        fetch(`/api/records`)
          // We get the API response and receive data in JSON format...
          .then(response => response.json())
          // ...then we update the users state
          .then(data =>
            
            this.setState({
              data: data,
              
            })
            
          )
          // Catch any errors we hit and update the app
          .catch(error => this.setState({ error, isLoading: false }));
          
      }

    render() {
        const columns = [{  
            Header: 'Id',  
            accessor: 'id'  
            },
            {  
            Header: 'Color',  
            accessor: 'color'  
            },
            {
            Header: 'disposition',  
            accessor: 'disposition'  
            }
        ] 
        const  {data} = this.props
        return (
          
            <div>
                <ReactTable  
                  data={data}  
                  columns={columns}  
                  defaultPageSize = {2}  
                  pageSizeOptions = {[2,4, 6]}  
              />  
            </div>
        )
    }
}
