const express = require("express");
const bodyParser = require("body-parser");
const managedRecords = require('./routes/api/managed-records')

const app = express();

// Bodyparser middleware
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());

app.use('/api', managedRecords);

app.use('/test', (req, res)=>{
res.send('hi')
})





const port = process.env.PORT || 5000; // process.env.port is Heroku's port if you choose to deploy the app there
app.listen(port, () => console.log(`Server up and running on port ${port} !`));
