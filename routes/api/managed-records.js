const express = require("express")
const router = express.Router()
let jsonData = require('../../data.js')


router.get('/records', (req, res)=>{

    res.send(JSON.stringify(jsonData.data))


} )

module.exports = router;